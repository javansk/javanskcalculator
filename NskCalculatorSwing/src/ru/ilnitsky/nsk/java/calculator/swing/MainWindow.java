package ru.ilnitsky.nsk.java.calculator.swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import ru.ilnitsky.nsk.java.calculator.core.*;

/**
 * Программа "Строковый калькулятор" с графическим интерфейсом на Swing
 */

public class MainWindow {
    private final String operatorsTextList = " Список поддерживаемых операторов:  (  )  -  +  *  /  %  \\  ^";
    private final String aboutText = " NskCalculator - простой строковый калькулятор.\n" +
            " М.Ильницкий, Новосибирск, 2019.\n" +
            " Список поддерживаемых операторов:\n" +
            "     (  )         -скобки\n" +
            "     -  +  *  /   -стандартные операции\n" +
            "     \\            -деление нацело\n" +
            "     %            -остаток от деления нацело\n" +
            "     ^            -возведение в степень\n\n";

    private final JFrame frame = new JFrame("NskCalculator - строковый калькулятор");
    private final JMenuBar menuBar = new JMenuBar();
    private final JTextField inputText = new JTextField();
    private final JLabel errorLabel = new JLabel(operatorsTextList);
    private final JTextArea outputArea = new JTextArea();

    private final LinkedList<String> expressionHistory = new LinkedList<>();
    private final LinkedList<String> resultHistory = new LinkedList<>();
    private ListIterator expressionsIterator = expressionHistory.listIterator();
    private ListIterator resultsIterator = resultHistory.listIterator();

    private final Calculator calculator;

    private void calc() {
        inputText.requestFocusInWindow();

        try {
            if (inputText.getText().isEmpty()) {
                return;
            }

            String result = calculator.calcResultString(inputText.getText());
            String expression = calculator.getExpressionString();

            outputArea.append(" --> " + expression);
            outputArea.append("\n");
            outputArea.append(" ans = " + result);
            outputArea.append("\n\n");

            expressionHistory.addFirst(expression);
            expressionsIterator = expressionHistory.listIterator();

            resultHistory.addFirst(result);
            resultsIterator = resultHistory.listIterator();

            errorLabel.setForeground(Color.BLUE);
            errorLabel.setText(operatorsTextList);
            inputText.setText("");

        } catch (RuntimeException ex) {
            errorLabel.setForeground(Color.RED);
            errorLabel.setText("Ошибка ввода выражения!    " + ex.getMessage());
        }
    }

    private void viewLastExpressionUp() {
        inputText.requestFocusInWindow();
        if (expressionsIterator.hasNext()) {
            inputText.setText((String) expressionsIterator.next());
        }
    }

    private void viewLastExpressionDown() {
        inputText.requestFocusInWindow();
        if (expressionsIterator.hasPrevious()) {
            inputText.setText((String) expressionsIterator.previous());
        }
    }

    private void viewLastResultUp() {
        inputText.requestFocusInWindow();
        if (resultsIterator.hasNext()) {
            inputText.setText((String) resultsIterator.next());
        }
    }

    private void viewLastResultDown() {
        inputText.requestFocusInWindow();
        if (resultsIterator.hasPrevious()) {
            inputText.setText((String) resultsIterator.previous());
        }
    }

    private MainWindow(Calculator calculatorObject) {
        calculator = calculatorObject;

        Image image = new ImageIcon(getClass().getResource("/ru/ilnitsky/nsk/java/calculator/swing/resources/JavaCalculator.png")).getImage();
        frame.setIconImage(image);

        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                exitDialog();
            }
        });
        frame.setLocationRelativeTo(null);

        initMenuBar();

        JScrollPane scrollPane = new JScrollPane(outputArea);
        JPanel mainPanel = new JPanel();
        JPanel bottomPanel = new JPanel();
        JPanel buttonPanel = new JPanel();

        JButton lastUpExpressionButton = new JButton("Пред. выражение");
        JButton lastDownExpressionButton = new JButton("След. выражение");
        JButton lastUpResultButton = new JButton("Пред. результат");
        JButton lastDownResultButton = new JButton("След. результат");
        JButton okButton = new JButton("Выполнить");

        frame.add(mainPanel, BorderLayout.CENTER);
        frame.add(bottomPanel, BorderLayout.SOUTH);

        mainPanel.setLayout(new GridLayout(1, 1));
        mainPanel.add(scrollPane);

        bottomPanel.setLayout(new GridLayout(3, 1));
        bottomPanel.add(errorLabel);
        bottomPanel.add(inputText);
        bottomPanel.add(buttonPanel);

        buttonPanel.setLayout(new GridLayout(1, 5));
        buttonPanel.add(lastUpExpressionButton);
        buttonPanel.add(lastDownExpressionButton);
        buttonPanel.add(okButton);
        buttonPanel.add(lastUpResultButton);
        buttonPanel.add(lastDownResultButton);

        errorLabel.setForeground(Color.BLUE);

        outputArea.setEditable(false);
        outputArea.setLineWrap(true);
        outputArea.setWrapStyleWord(true);
        outputArea.setFont(new Font("Courier New", Font.PLAIN, 18));
        outputArea.setText(aboutText);

        okButton.addActionListener(e -> calc());

        inputText.addActionListener(e -> calc());

        lastUpExpressionButton.addActionListener(e -> viewLastExpressionUp());
        lastDownExpressionButton.addActionListener(e -> viewLastExpressionDown());
        lastUpResultButton.addActionListener(e -> viewLastResultUp());
        lastDownResultButton.addActionListener(e -> viewLastResultDown());

        inputText.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_UP) {
                    viewLastExpressionUp();
                } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                    viewLastExpressionDown();
                } else if (e.getKeyCode() == KeyEvent.VK_PAGE_UP) {
                    viewLastResultUp();
                } else if (e.getKeyCode() == KeyEvent.VK_PAGE_DOWN) {
                    viewLastResultDown();
                }
            }
        });
    }

    private void setVisible() {
        frame.setVisible(true);
        inputText.requestFocusInWindow();
    }

    private void initMenuBar() {
        JMenu menuFile = new JMenu("Файл");

        JMenuItem itemClear = new JMenuItem("Очистить историю");
        itemClear.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        itemClear.addActionListener(e -> {
            outputArea.setText(aboutText);
            inputText.requestFocusInWindow();
        });
        menuFile.add(itemClear);

        JMenuItem itemExit = new JMenuItem("Выход");
        itemExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, InputEvent.CTRL_MASK));
        itemExit.addActionListener(e -> exitDialog());
        menuFile.add(itemExit);

        JMenu menuAbout = new JMenu("О программе");
        JMenuItem itemAbout = new JMenuItem("О программе");
        itemAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, InputEvent.CTRL_MASK));
        itemAbout.addActionListener(e ->
                JOptionPane.showMessageDialog(frame,
                        new String[]{"NskCalculator",
                                "Строковый калькулятор",
                                "Графический интерфейс на основе Swing",
                                "М.Ильницкий, Новосибирск, 2019"},
                        "О программе",
                        JOptionPane.INFORMATION_MESSAGE)
        );
        menuAbout.add(itemAbout);

        menuBar.add(menuFile);
        menuBar.add(menuAbout);

        frame.setJMenuBar(menuBar);
    }

    private void exitDialog() {
        try {
            Object[] options = {"Да", "Нет"};
            int result = JOptionPane.showOptionDialog(frame,
                    "Завершить программу?",
                    "Подтверждение завершения",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

            if (result == JOptionPane.OK_OPTION) {
                System.exit(0);
            }
        } catch (Exception exception) {
            exception.getStackTrace();
        }
    }

    public static void main(String[] args) {
        MainWindow mainWindow = new MainWindow(new SimpleCalculator());
        mainWindow.setVisible();
    }
}
