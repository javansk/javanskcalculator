package ru.ilnitsky.nsk.java.calculator.core;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Класс с функцией main для тестирования класса SimpleSorter
 */

class SorterTest {
    public static void main(String[] args) {
        System.out.println();
        System.out.println("*************************************");
        System.out.println("Тест SimpleSorter.convertToPostfixNotation:");
        System.out.println();

        Corrector corrector = new SimpleCorrector();
        Parser parser = new SimpleParser();
        Sorter sorter = new SimpleSorter();

        String[] strings = {
                "5*2+10",                       // 5, 2, *, 10, +
                "(6+10-4)/(1+1*2)+1",           // 6, 10, +, 4, -, 1, 1, 2, *, +, /, 1, +
                "(8+2*5)/(1+3*2-4)",            // 8, 2, 5, *, +, 1, 3, 2, *, +, 4, -, /
                "1+2-3*5-4/3-5^2+10\\3+10%3",
                " ( 1+ 3/ 5 )/(7-4^2) ",
                "((-1-2)*(+3-4)-5)%4e7",
                "1.2e5-3/7"};

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                LinkedList<Token> infixTokens = parser.parse(corrector.correct(s));

                for (Token t : infixTokens) {
                    switch (t.getType()) {
                        case INTEGER:
                            System.out.print("i[");
                            break;
                        case FLOAT:
                            System.out.print("f[");
                            break;
                        default:
                            System.out.print("[");
                    }
                    System.out.print(t.toString() + "] ");
                }
                System.out.println();

                Queue<Token> postfixTokens = sorter.convertToPostfixNotation(infixTokens);

                for (Token t : postfixTokens) {
                    switch (t.getType()) {
                        case INTEGER:
                            System.out.print("i[");
                            break;
                        case FLOAT:
                            System.out.print("f[");
                            break;
                        default:
                            System.out.print("[");
                    }
                    System.out.print(t.toString() + "] ");
                }
                System.out.println();

            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }
}
