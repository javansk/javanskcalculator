package ru.ilnitsky.nsk.java.calculator.core;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Класс для преобразования списка токенов от инфиксного порядка к постфиксному порядку
 * Т.е. обычный порядок операндов преобразуется в обратную польскую нотацию
 * Используется алгоритм "сортировочной станции" Дейкстры
 */

public class SimpleSorter implements Sorter {

    public Queue<Token> convertToPostfixNotation(Queue<Token> inputInfixTokens) {
        Queue<Token> queueOutputTokens = new LinkedList<>();
        Deque<Token> stackOperators = new LinkedList<>();

        while (!inputInfixTokens.isEmpty()) {
            if (inputInfixTokens.element().getType() != TokenType.OPERATOR) {
                queueOutputTokens.add(inputInfixTokens.remove());
                //System.out.println("1");
                //System.out.println("inputInfixTokens:" + inputInfixTokens);
                //System.out.println("stackOperators:" + stackOperators);
                //System.out.println("queueOutputTokens" + queueOutputTokens);
            } else {
                if (stackOperators.isEmpty()) {
                    stackOperators.addLast(inputInfixTokens.remove());
                    //System.out.println("2");
                    //System.out.println("inputInfixTokens:" + inputInfixTokens);
                    //System.out.println("stackOperators:" + stackOperators);
                    //System.out.println("queueOutputTokens" + queueOutputTokens);
                } else {
                    if (inputInfixTokens.element().getOperator() == Operator.RIGHT_BRACKET) {
                        inputInfixTokens.remove();
                        while (stackOperators.getLast().getOperator() != Operator.LEFT_BRACKET) {
                            queueOutputTokens.add(stackOperators.removeLast());
                            //System.out.println("3");
                            //System.out.println("inputInfixTokens:" + inputInfixTokens);
                            //System.out.println("stackOperators:" + stackOperators);
                            //System.out.println("queueOutputTokens" + queueOutputTokens);

                            if (stackOperators.isEmpty()) {
                                throw new RuntimeException("No left bracket in Stack!");
                            }
                        }
                        stackOperators.removeLast();
                    } else {
                        while (!stackOperators.isEmpty()) {
                            if (stackOperators.getLast().getOperator() == Operator.LEFT_BRACKET) {
                                break;
                            } else if (inputInfixTokens.element().getOperator() != Operator.EXPONENTIATION) {
                                if (stackOperators.getLast().getOperator().getPriority() >= inputInfixTokens.element().getOperator().getPriority()) {
                                    queueOutputTokens.add(stackOperators.removeLast());
                                    //System.out.println("4");
                                    //System.out.println("inputInfixTokens:" + inputInfixTokens);
                                    //System.out.println("stackOperators:" + stackOperators);
                                    //System.out.println("queueOutputTokens" + queueOutputTokens);
                                } else {
                                    break;
                                }
                            } else {
                                if (stackOperators.getLast().getOperator().getPriority() > inputInfixTokens.element().getOperator().getPriority()) {
                                    queueOutputTokens.add(stackOperators.removeLast());
                                    //System.out.println("5");
                                    //System.out.println("inputInfixTokens:" + inputInfixTokens);
                                    //System.out.println("stackOperators:" + stackOperators);
                                    //System.out.println("queueOutputTokens" + queueOutputTokens);
                                } else {
                                    break;
                                }
                            }
                        }
                        stackOperators.addLast(inputInfixTokens.remove());
                        //System.out.println("6");
                        //System.out.println("inputInfixTokens:" + inputInfixTokens);
                        //System.out.println("stackOperators:" + stackOperators);
                        //System.out.println("queueOutputTokens" + queueOutputTokens);
                    }

                }
            }
        }
        while (!stackOperators.isEmpty()) {
            queueOutputTokens.add(stackOperators.removeLast());
            //System.out.println("7");
            //System.out.println("inputInfixTokens:" + inputInfixTokens);
            //System.out.println("stackOperators:" + stackOperators);
            //System.out.println("queueOutputTokens" + queueOutputTokens);
        }

        return queueOutputTokens;
    }
}
