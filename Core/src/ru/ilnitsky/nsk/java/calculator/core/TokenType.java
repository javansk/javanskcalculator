package ru.ilnitsky.nsk.java.calculator.core;

/**
 * Перечисление содержащие типы токенов
 */

public enum TokenType {
    INTEGER,
    FLOAT,
    OPERATOR
}
