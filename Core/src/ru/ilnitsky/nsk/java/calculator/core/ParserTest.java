package ru.ilnitsky.nsk.java.calculator.core;

import java.util.LinkedList;

/**
 * Класс с функцией main для тестирования класса SimpleParser
 */

class ParserTest {
    public static void main(String[] args) {
        System.out.println();
        System.out.println("******************");
        System.out.println("Тест SimpleParser.parse:");
        System.out.println();

        Corrector corrector = new SimpleCorrector();
        Parser parser = new SimpleParser();

        String[] strings = {
                "1+2-3*5-4/3-5^2+10\\3+10%3",
                " ( 1+ 3/ 5 )/(7-4^2) ",
                "((-1-2)*(+3-4)-5)%4e7",
                "1.2e5-3/7",
                "1.2.3e7-1",
                "1e4e5+1e12",
                "3-4-5-",
                "3/+5-6//7",
                "- (1+2) /5+67-5%2"};

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                LinkedList<Token> tokens = parser.parse(corrector.correct(s));
                for (Token t : tokens) {
                    switch (t.getType()) {
                        case INTEGER:
                            System.out.print("i[");
                            break;
                        case FLOAT:
                            System.out.print("f[");
                            break;
                        default:
                            System.out.print("[");
                    }
                    System.out.print(t.toString() + "] ");
                }
                System.out.println();
            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }
}
