package ru.ilnitsky.nsk.java.calculator.core;

import java.util.Queue;

/**
 * Интерефейс класса преобразования списка токенов арифметического выражения из инфиксной нотации в постфиксную
 */

public interface Sorter {
    Queue<Token> convertToPostfixNotation(Queue<Token> inputInfixTokens);
}
