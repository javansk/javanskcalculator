package ru.ilnitsky.nsk.java.calculator.core;

/**
 * Класс содержащий одну из возможных лексем арифметического выражения
 */

public class Token {
    private final TokenType type;

    private final long intValue;
    private final double floatValue;
    private final Operator operator;

    public Token(long intValue) {
        this.intValue = intValue;

        type = TokenType.INTEGER;

        floatValue = 0;
        operator = null;
    }

    public Token(double floatValue) {
        this.floatValue = floatValue;

        type = TokenType.FLOAT;

        intValue = 0;
        operator = null;
    }

    public Token(Operator operator) {
        this.operator = operator;

        type = TokenType.OPERATOR;

        intValue = 0;
        floatValue = 0;
    }

    public TokenType getType() {
        return type;
    }

    public long getIntValue() {
        if (type != TokenType.INTEGER) {
            throw new UnsupportedOperationException("Token is not INTEGER! Type of token: " + type);
        }
        return intValue;
    }

    public double getFloatValue() {
        if (type != TokenType.FLOAT) {
            throw new UnsupportedOperationException("Token is not FLOAT! Type of token: " + type);
        }
        return floatValue;
    }

    public Operator getOperator() {
        if (type != TokenType.OPERATOR) {
            throw new UnsupportedOperationException("Token is not OPERATOR! Type of token: " + type);
        }
        return operator;
    }

    @Override
    public String toString() {
        switch (type) {
            case OPERATOR:
                return "" + operator.getSymbol();
            case INTEGER:
                return Long.toString(intValue);
            case FLOAT:
                return Double.toString(floatValue);
            default:
                return "";
        }
    }
}
