package ru.ilnitsky.nsk.java.calculator.core;

import java.util.Queue;

/**
 * Интерфейс процессора обрабатывающего арифметическое выражение в виде списка токенов в постфиксной нотации
 */

public interface Processor {
    Token calc(Queue<Token> inputPostfixTokens);
}
