package ru.ilnitsky.nsk.java.calculator.core;

import java.util.LinkedList;

/**
 * Класс для преобразования входной строки арифметического выражения в массив токенов
 */

public class SimpleParser implements Parser {

    // Создаём токен целого или дробного числа
    private Token createValueToken(String string) {
        long intValue;
        double floatValue;

        Token token;

        try {
            intValue = Long.parseLong(string);
            token = new Token(intValue);
        } catch (NumberFormatException e1) {
            try {
                floatValue = Double.parseDouble(string);
                token = new Token(floatValue);
            } catch (NumberFormatException e2) {
                throw new IllegalArgumentException("Error Number Format in String [" + string + "]");
            }
        }

        return token;
    }

    // Преобразуем входную строку символов в список токенов в исходном (инфиксном) порядке их следования
    public LinkedList<Token> parse(String string) {
        char[] symbols = string.toCharArray();

        LinkedList<Token> tokens = new LinkedList<>();

        StringBuilder valueString = new StringBuilder();

        for (char s : symbols) {
            try {
                Operator operator = Operator.getType(s);

                if (valueString.length() != 0) {
                    tokens.add(createValueToken(valueString.toString()));
                }

                tokens.add(new Token(operator));

                valueString = new StringBuilder();
            } catch (IllegalArgumentException e) {
                valueString.append(s);
            }
        }

        if (valueString.length() != 0) {
            tokens.add(createValueToken(valueString.toString()));
        }

        return tokens;
    }
}
