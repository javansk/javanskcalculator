package ru.ilnitsky.nsk.java.calculator.core;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Класс с функцией main для тестирования класса SimpleProcessor
 */

class ProcessorTest {
    public static void main(String[] args) {
        System.out.println();
        System.out.println("*************************************");
        System.out.println("Тест SimpleSorter.convertToPostfixNotation:");
        System.out.println();

        Corrector corrector = new SimpleCorrector();
        Parser parser = new SimpleParser();
        Sorter sorter = new SimpleSorter();
        Processor processor = new SimpleProcessor();

        String[] strings = {
                "5*2+10",                           // 20   // 5, 2, *, 10, +
                "(6+10-4)/(1+1*2)+1",               //  5   // 6, 10, +, 4, -, 1, 1, 2, *, +, /, 1, +
                "(8+2*5)/(1+3*2-4)",                //  6   // 8, 2, 5, *, +, 1, 3, 2, *, +, 4, -, /
                "1+2-3*5-4/3-5^2+10\\3+10%3",       // -34,333333333333333333333333333333
                " ( 1+ 3/ 5 )/(7-4^2) ",            // -0,17777777777777777777777777777778
                "((-1-2)*(+3-4)-5)%4e7",            // -2
                "1.2e1-3/7",                        // 11,571428571428571428571428571429
                "5%2",              //   1
                "4%1.3",            //   0.1
                "5\\2",             //   2
                "4\\1.3",           //   3
                "2^(3^2)",          // 512
                "(2^3)^2",          //  64
                "2^3^2"};           // 512

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                LinkedList<Token> infixTokens = parser.parse(corrector.correct(s));

                for (Token t : infixTokens) {
                    switch (t.getType()) {
                        case INTEGER:
                            System.out.print("i[" + t.toString() + "] ");
                            break;
                        case FLOAT:
                            System.out.print("f[" + t.toString() + "] ");
                            break;
                        default:
                            System.out.print(t.toString() + " ");
                    }
                }
                System.out.println();

                Queue<Token> postfixTokens = sorter.convertToPostfixNotation(infixTokens);

                for (Token t : postfixTokens) {
                    switch (t.getType()) {
                        case INTEGER:
                            System.out.print("i[" + t.toString() + "] ");
                            break;
                        case FLOAT:
                            System.out.print("f[" + t.toString() + "] ");
                            break;
                        default:
                            System.out.print(t.toString() + " ");
                    }
                }
                System.out.println();

                Token result = processor.calc(postfixTokens);
                System.out.println("Result = " + result);


            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }
}
