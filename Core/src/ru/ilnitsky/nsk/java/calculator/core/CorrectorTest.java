package ru.ilnitsky.nsk.java.calculator.core;

import java.util.regex.Pattern;

/**
 * Класс с функцией main для тестирования функций класса SimpleCorrector
 */

class CorrectorTest extends SimpleCorrector {
    private static void testSimplification(String[] strings) {
        System.out.println();
        System.out.println("********************");
        System.out.println("Тест simplification:");
        System.out.println();

        Corrector corrector = new SimpleCorrector();

        for (String s : strings) {
            System.out.println("Input:" + s);
            System.out.println("Output:" + corrector.simplification(s));
            System.out.println();
        }
    }

    private static void testValidationOfSymbols(String[] strings) {
        char[] symbols = SimpleCorrector.getAllowableSymbols();

        System.out.println();
        System.out.println("*************************");
        System.out.println("Тест validationOfSymbols:");
        System.out.println();

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                SimpleCorrector.validationOfSymbols(s, symbols);
            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }

    private static void testValidationOfBeginAndEnd(String[] strings) {
        System.out.println();
        System.out.println("*****************************");
        System.out.println("Тест validationOfBeginAndEnd:");
        System.out.println();

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                SimpleCorrector.validationOfBeginAndEnd(s);
            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }

    private static void testValidationOfBrackets(String[] strings) {
        System.out.println();
        System.out.println("**************************");
        System.out.println("Тест validationOfBrackets:");
        System.out.println();

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                SimpleCorrector.validationOfBrackets(s);
            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }

    private static void testDeleteUnary(String[] strings) {
        System.out.println();
        System.out.println("*****************");
        System.out.println("Тест deleteUnary:");
        System.out.println();

        for (String s : strings) {
            System.out.println("Input:" + s);
            System.out.println("Output:" + SimpleCorrector.deleteUnary(s));
            System.out.println();
        }
    }

    private static void testCreatePatternStringOfImpossibleOperatorCombinations() {
        System.out.println();
        System.out.println("*********************************************************");
        System.out.println("Тест createPatternStringOfImpossibleOperatorCombinations:");
        System.out.println();

        System.out.println("Pattern String:" + SimpleCorrector.createPatternStringOfImpossibleOperatorCombinations());
    }

    private static void testValidationOfOperatorCombinations(String[] strings) {
        System.out.println();
        System.out.println("**************************************");
        System.out.println("Тест validationOfOperatorCombinations:");
        System.out.println();

        Pattern pattern = SimpleCorrector.createPatternOfImpossibleOperatorCombinations(SimpleCorrector.createPatternStringOfImpossibleOperatorCombinations());

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                SimpleCorrector.validationOfOperatorCombinations(s, pattern);
            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }

    private static void testCorrect(String[] strings) {
        System.out.println();
        System.out.println("*****************************");
        System.out.println("Тест SimpleCorrector.correct:");
        System.out.println();

        Corrector corrector = new SimpleCorrector();

        for (String s : strings) {
            System.out.println("Input:" + s);
            try {
                System.out.println("Output:" + corrector.correct(s));
            } catch (IllegalArgumentException e) {
                System.out.println("Exception: " + e.getMessage());
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        String[] testSimplification = {" (1+2) + (3-5)", "1,5/5", "1.1e4*2,3", " - 1  + 2 * 5 "};
        testSimplification(testSimplification);

        String[] testValidationOfSymbols = {"1+2+3*4/2%3", " (1+2) + (3-5e4)", "1,5/5", "1.1d-5", "[1+2]*7", "1-{3-7}"};
        testValidationOfSymbols(testValidationOfSymbols);

        String[] testValidationOfBeginAndEnd = {"-1+5", "+1-3", "^2+5", "/3-5", "*7*8*9", " (1+2) + (3-5)", "1,5/5-", "1.1e4*2,3*", " - 1  + 2 * 5 /", "(1+2)", "(5*(1-2))"};
        testValidationOfBeginAndEnd(testValidationOfBeginAndEnd);

        String[] testValidationOfBrackets = {"(1+2)+(3-5)", "((1+5)/5)", "(1.1e4*2.3", "(1  + 2) * 5)"};
        testValidationOfBrackets(testValidationOfBrackets);

        String[] testDeleteUnary = {"-1+2*(-3-5)", "(+5/5)", "+1.1e4*2,3", "-1+2*(-5)"};
        testDeleteUnary(testDeleteUnary);

        testCreatePatternStringOfImpossibleOperatorCombinations();

        String[] testValidationOfOperatorCombinations = {"(1+2*(3-5)^3-5)", "(+5/5)", "3+-7", "2/*3", "5*-5", "()(3+5)", ")(3-7", "(((3+(5\\2-1/2%1+2^5))))"};
        testValidationOfOperatorCombinations(testValidationOfOperatorCombinations);

        String[] testCorrect = {" (1+2) + (3-5)", "1,5/5", "1.1e4*2,3", " - 1  + 2 * 5 ", "1+2+3*4/2%3", " (1+2) + (3-5e4)", "1,5/5", "1.1d-5", "[1+2]*7", "1-{3-7}", "-1+2*(-3-5)", "(+5/5)", "+1.1e4*2,3", "-1+2*(-5)", "(1+2*(3-5)^3-5)", "(+5/5)", "3+-7", "2/*3", "5*-5", "()(3+5)", ")(3-7", "(((3+(5\\2-1/2%1+2^5))))"};
        testCorrect(testCorrect);
    }
}
