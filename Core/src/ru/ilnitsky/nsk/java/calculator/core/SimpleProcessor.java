package ru.ilnitsky.nsk.java.calculator.core;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Класс для вычисления значения входного выражения, представленного в виде списка токенов
 * в постфиксной нотации (т.е. в виде обратной польской записи)
 */

public class SimpleProcessor implements Processor {
    private Deque<Token> stack;

    private void calcForStack() {
        long maxInt = (long) 9e18;

        Operator operator = stack.removeLast().getOperator();
        Token tokenB = stack.removeLast();
        Token tokenA = stack.removeLast();
        Token result;

        if (tokenA.getType() == TokenType.FLOAT || tokenB.getType() == TokenType.FLOAT) {
            double a;
            double b;

            if (tokenA.getType() == TokenType.FLOAT) {
                a = tokenA.getFloatValue();
            } else {
                a = tokenA.getIntValue();
            }
            if (tokenB.getType() == TokenType.FLOAT) {
                b = tokenB.getFloatValue();
            } else {
                b = tokenB.getIntValue();
            }

            if (operator == Operator.INT_DIVISION) {
                result = new Token(operator.calcInt(a, b));
            } else {
                result = new Token(operator.calcFloat(a, b));

                double r = result.getFloatValue();
                long i = (long) r;
                if (Math.abs(r) < maxInt && r % i == 0) {
                    result = new Token(i);
                }
            }

        } else {
            long a = tokenA.getIntValue();
            long b = tokenB.getIntValue();

            if ((operator == Operator.DIVISION) && (a % b != 0)) {
                result = new Token(operator.calcFloat(a, b));
            } else {
                result = new Token(operator.calcInt(a, b));
            }

            if (result.getType() == TokenType.INTEGER && Math.abs(result.getIntValue()) > maxInt) {
                result = new Token(operator.calcFloat(a, b));
            }
        }

        stack.addLast(result);
    }

    public Token calc(Queue<Token> inputPostfixTokens) {
        stack = new LinkedList<>();

        while (!inputPostfixTokens.isEmpty()) {
            stack.addLast(inputPostfixTokens.remove());

            if (stack.getLast().getType() == TokenType.OPERATOR) {
                calcForStack();
            }
        }

        return stack.removeLast();
    }
}
