package ru.ilnitsky.nsk.java.calculator.core;

/**
 * Интерфейс калькулятора принимающего на вход строку с арифметическим выражением и вычисляющего результат
 */

public interface Calculator {
    Token calcResultToken(String inputString);

    String calcResultString(String inputString);

    String getExpressionString();
}
