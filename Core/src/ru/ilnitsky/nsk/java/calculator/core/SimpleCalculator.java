package ru.ilnitsky.nsk.java.calculator.core;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Класс для вычисления значения арифметического выражения представленного в виде строки текста
 */

public class SimpleCalculator implements Calculator {
    private final Corrector corrector;
    private final Parser parser;
    private final Sorter sorter;
    private final Processor processor;
    private String expressionString;

    public SimpleCalculator(Corrector corrector, Parser parser, Sorter sorter, Processor processor) {
        this.corrector = corrector;
        this.parser = parser;
        this.sorter = sorter;
        this.processor = processor;
    }

    public SimpleCalculator() {
        this(new SimpleCorrector(), new SimpleParser(), new SimpleSorter(), new SimpleProcessor());
    }

    public Token calcResultToken(String inputString) {
        expressionString = "";

        LinkedList<Token> infixTokens = parser.parse(corrector.correct(inputString));
        Queue<Token> postfixTokens = sorter.convertToPostfixNotation(infixTokens);

        expressionString = corrector.simplification(inputString);

        return processor.calc(postfixTokens);
    }

    public String calcResultString(String inputString) {
        return calcResultToken(inputString).toString();
    }

    public String getExpressionString() {
        return expressionString;
    }
}
