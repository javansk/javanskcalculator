package ru.ilnitsky.nsk.java.calculator.core;

/**
 * Интерфейс класса проверяющего и корректирующего входную строку арифметических выражений
 */

public interface Corrector {
    String simplification(String string);

    String correct(String inputString);
}
