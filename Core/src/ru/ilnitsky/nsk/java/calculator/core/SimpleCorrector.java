package ru.ilnitsky.nsk.java.calculator.core;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс для коррекции и валидации входной строки арифметических выражений
 */

public class SimpleCorrector implements Corrector {
    private final char[] allowableSymbols;
    private final Pattern patternOfImpossibleOperatorCombinations;

    public SimpleCorrector() {
        allowableSymbols = SimpleCorrector.getAllowableSymbols();
        patternOfImpossibleOperatorCombinations = SimpleCorrector.createPatternOfImpossibleOperatorCombinations(SimpleCorrector.createPatternStringOfImpossibleOperatorCombinations());
    }

    // Удаляем лишние пробелы и заменяем русские символы на международные
    public String simplification(String string) {
        return string.trim().toLowerCase().replaceAll(",", ".").replaceAll("е", "e").replaceAll(" ", "");
    }

    // Создаём список допустимых символов
    protected static char[] getAllowableSymbols() {
        char[] valueSymbols = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', 'e'};
        char[] operatorSymbols = Operator.getAllSymbols();

        int valueLength = valueSymbols.length;
        int operatorLength = operatorSymbols.length;
        int length = valueLength + operatorLength;

        char[] symbols = Arrays.copyOf(valueSymbols, length);
        System.arraycopy(operatorSymbols, 0, symbols, valueLength, operatorLength);

        return symbols;
    }

    // Проверяем входную строку на наличие недопустимых символов
    protected static void validationOfSymbols(String string, char[] allowableSymbols) {
        if (string.isEmpty()) {
            throw new IllegalArgumentException("Empty String!");
        }

        char[] stringSymbols = string.toCharArray();

        outer:
        for (char s : stringSymbols) {
            for (char a : allowableSymbols) {
                if (s == a) {
                    continue outer;
                }
            }

            throw new IllegalArgumentException("Illegal Symbol: [" + s + "]");
        }
    }

    // Проверяем входную строку на наличие операторов в начале и в конце выражения
    protected static void validationOfBeginAndEnd(String string) {
        char beginSymbol = string.charAt(0);
        char endSymbol = string.charAt(string.length() - 1);

        char[] operatorSymbols = Operator.getSymbolsWithoutBrackets();

        for (char o : operatorSymbols) {
            if (beginSymbol == o && o != '-' && o != '+') {
                throw new IllegalArgumentException("Illegal Begin Symbol: [" + beginSymbol + "]");
            }
            if (endSymbol == o) {
                throw new IllegalArgumentException("Illegal End Symbol: [" + endSymbol + "]");
            }
        }
    }

    // Проверяем входную строку на равенство количества открывающих и закрывающих скобок
    protected static void validationOfBrackets(String string) {
        char[] stringSymbols = string.toCharArray();

        int leftBrackets = 0;
        int rightBrackets = 0;
        for (char s : stringSymbols) {
            if (s == '(') {
                leftBrackets++;
            }
            if (s == ')') {
                rightBrackets++;
            }
        }

        if (leftBrackets != rightBrackets) {
            throw new IllegalArgumentException("Numbers of left(" + leftBrackets + ") and right(" + rightBrackets + ") brackets are unequal!");
        }
    }

    // Удаляем унарные плюсы и заменяем унарные минусы на бинарные
    protected static String deleteUnary(String string) {
        if (string.charAt(0) == '+') {
            string = string.substring(1);
        } else if (string.charAt(0) == '-') {
            string = "0" + string;
        }

        string = string.replace("(+", "(").replace("(-", "(0-");

        return string;
    }

    // Создаём строку шаблона недопустимых сочетаний операторов
    protected static String createPatternStringOfImpossibleOperatorCombinations() {
        char[] operatorChars = Operator.getSymbolsWithoutBrackets();
        StringBuilder operatorsStringBuilder = new StringBuilder();

        operatorsStringBuilder.append("*");
        for (char c : operatorChars) {
            if (c != '*') {
                operatorsStringBuilder.append(c);
            }
        }
        String operatorsString = operatorsStringBuilder.toString();

        return new StringBuilder()
                .append("[\\(][\\)]")
                .append("|[\\)][\\(]")
                .append("|[\\(][").append(operatorsString).append("]")
                .append("|[").append(operatorsString).append("][\\)]")
                .append("|[").append(operatorsString).append("]{2}")
                .toString();
    }

    // Создаём шаблон недопустимых сочетаний операторов
    protected static Pattern createPatternOfImpossibleOperatorCombinations(String patternString) {
        return Pattern.compile(patternString);
    }

    // Проверяем входную строку на недопустимое соседство операторов
    protected static void validationOfOperatorCombinations(String string, Pattern pattern) {
        Matcher matcher = pattern.matcher(string);

        if (matcher.find()) {
            throw new IllegalArgumentException("Illegal Operator Combination!");
        }
    }

    public String correct(String inputString) {
        // Удаляем лишние пробелы и заменяем русские символы на международные
        String string = simplification(inputString);

        // Проверяем входную строку на наличие недопустимых символов
        validationOfSymbols(string, allowableSymbols);

        // Проверяем входную строку на наличие операторов в конце выражения
        validationOfBeginAndEnd(string);

        // Проверяем входную строку на равенство количества открывающих и закрывающих скобок
        validationOfBrackets(string);

        // Удаляем унарные плюсы и заменяем унарные минусы на бинарные
        string = deleteUnary(string);

        // Проверяем входную строку на недопустимое соседство операторов
        validationOfOperatorCombinations(string, patternOfImpossibleOperatorCombinations);

        return string;
    }
}
