package ru.ilnitsky.nsk.java.calculator.core;

/**
 * Перечесление содержащее типы операторов
 */

public enum Operator {
    PLUS(0, '+'),
    MINUS(0, '-'),
    MULTIPLICATION(1, '*'),
    DIVISION(1, '/'),
    INT_DIVISION(1, '\\'),
    REMAINDER(1, '%'),
    EXPONENTIATION(2, '^'),
    LEFT_BRACKET(3, '('),
    RIGHT_BRACKET(3, ')');

    private final int priority;
    private final char symbol;

    Operator(int priority, char symbol) {
        this.priority = priority;
        this.symbol = symbol;
    }

    public char getSymbol() {
        return this.symbol;
    }

    public int getPriority() {
        return this.priority;
    }

    public static char[] getSymbolsWithoutBrackets() {
        return new char[]{'+', '-', '*', '/', '\\', '%', '^'};
    }

    public static char[] getAllSymbols() {
        return new char[]{'+', '-', '*', '/', '\\', '%', '^', '(', ')'};
    }

    public static Operator getType(char symbol) {
        switch (symbol) {
            case '+':
                return PLUS;
            case '-':
                return MINUS;
            case '*':
                return MULTIPLICATION;
            case '/':
                return DIVISION;
            case '\\':
                return INT_DIVISION;
            case '%':
                return REMAINDER;
            case '^':
                return EXPONENTIATION;
            case '(':
                return LEFT_BRACKET;
            case ')':
                return RIGHT_BRACKET;
            default:
                throw new IllegalArgumentException("Unknown Operator:" + symbol);
        }
    }

    public static long calcInt(long a, long b, Operator operator) {
        switch (operator) {
            case PLUS:
                return a + b;
            case MINUS:
                return a - b;
            case MULTIPLICATION:
                return a * b;
            case DIVISION:
            case INT_DIVISION:
                return a / b;
            case REMAINDER:
                return a % b;
            case EXPONENTIATION:
                return (long) Math.pow(a, b);
            default:
                throw new IllegalArgumentException("Impossible Operator:" + operator);
        }
    }

    public static long calcInt(double a, double b, Operator operator) {
        if (operator == INT_DIVISION) {
            return (long) (a / b);
        } else {
            throw new IllegalArgumentException("Impossible Operator:" + operator);
        }
    }

    public static double calcFloat(double a, double b, Operator operator) {
        switch (operator) {
            case PLUS:
                return a + b;
            case MINUS:
                return a - b;
            case MULTIPLICATION:
                return a * b;
            case DIVISION:
                return a / b;
            case REMAINDER:
                return a % b;
            case EXPONENTIATION:
                return Math.pow(a, b);
            default:
                throw new IllegalArgumentException("Impossible Operator:" + operator);
        }
    }

    public long calcInt(long a, long b) {
        return calcInt(a, b, this);
    }

    public long calcInt(double a, double b) {
        return calcInt(a, b, this);
    }

    public double calcFloat(double a, double b) {
        return calcFloat(a, b, this);
    }
}
