package ru.ilnitsky.nsk.java.calculator.core;

import java.util.LinkedList;

/**
 * Интрефейс класса преобразующего входную строку с арифметическим выражением в массив токенов той же последовательности
 */

public interface Parser {
    LinkedList<Token> parse(String string);
}
